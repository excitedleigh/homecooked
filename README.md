# Homecooked

## What is this?

This is the Chef cookbook that sets up new computers the way I like them, by installing the software I want, and configuring some of it. Most of the configuration files are stored in separate git repositories, which this cookbook will clone into `~/config`.

## Can I use it?

Sure!

To the extent possible under law, Adam Brenecki has waived all copyright and related or neighbouring rights to the content of this repository, as set out in [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/).

## How do I use it?

```
# macOS only
xcode-select --install
sudo xcodebuild -license
# all platforms
mkdir ~/config
cd ~/config
git clone https://gitlab.com/abre/homecooked.git
cd homecooked
./run.sh
```


## What doesn't it do?

### On any OS

### OS X

- Install apps from the Mac App Store. (Fortunately, the MAS has a 'Purchased' tab that shows your purchase history, and you can even hide apps you didn't like from it to keep it cleaned up.)
- Run the Creative Cloud installer; to do this run `open "/usr/local/Caskroom/adobe-creative-cloud/latest/Creative Cloud Installer.app"`.
- Start up menubar apps that should run at startup. (They'll usually do this themselves when you first start them, or at least offer to.)
- Set Fish as the default shell (currently). To do that, run the following in Bash: `` echo `which fish` | sudo tee -a /etc/shells && chsh -s `which fish` ``

### Linux

Probably some things. Linux is much more friendly to non-interactive configuration, and my Linux setup isn't nearly as customised as my OS X one, though.

#### Debian

- Add your user to the sudo group
- Add contrib, non-free, jessie-backports to your sources.list
- Remove the pre-existing `.gpg` / `.ssh` directories

### Windows

Anything, at all.
