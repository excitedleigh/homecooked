home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

execute "git clone https://gitlab.com/abre/atomconfig.git #{home}/config/atom" do
  creates "#{home}/config/atom"
end

# Link in Atom config
link "#{home}/.atom" do
  to "#{home}/config/atom"
  action :create
end

if platform? "ubuntu" or platform? "debian"
  execute 'wget -O- https://www.franzoni.eu/keys/D1270819.txt | sudo apt-key add -'
  apt_repository 'atom' do
    uri "http://www.a9f.eu/apt/atom/#{node['platform']}"
    distribution node['lsb']['codename']
    components ['main']
  end
  package 'atom'
end
if platform_family? "mac_os_x"
  homebrew_cask 'atom'
  execute "defaults write com.github.atom ApplePressAndHoldEnabled -bool false"
end

# execute "apm install --packages-file #{home}/config/atom/packages.txt"
