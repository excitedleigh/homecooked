user = ENV['SUDO_USER'] || node['current_user']
home = Dir.home(user)

# Python packages are now in homecooked::python

# NodeJS packages
npmpath = "#{home}/.npm-global"
directory npmpath
file "#{home}/.npmrc" do
  content "prefix=#{npmpath}"
end

[
  'babel',
  'bower',
  'coffee-script',
  'coffeelint',
  'jsdoc',
  'jshint',
  'jsxhint',
  'less',
  'license',
  'react-native-cli',
  'rnpm',
  'nativefier',
  'tern',
  'typings',
  'tiddlywiki',
  'ungit',
].each do |pkg|
  execute "npm install -g #{pkg}" do
    creates "#{npmpath}/lib/node_modules/#{pkg}"
  end
end

# Rubygems
[
  'boom',
  'foreman',
  'mailcatcher',
  'lolcommits',
].each do |pkg|
  gem_package pkg
end

# Go packages
gopath = "#{home}/.go-global"
directory gopath
[
  "github.com/golang/lint/golint",
  "github.com/rogpeppe/godef",
  "github.com/nsf/gocode",
  "github.com/lukehoban/go-find-references",
  "github.com/lukehoban/go-outline",
  "sourcegraph.com/sqs/goreturns",
  "golang.org/x/tools/cmd/gorename",
  "github.com/tpng/gopkgs",
  "github.com/newhook/go-symbols",
  "github.com/cortesi/modd/cmd/modd",
  "github.com/cortesi/devd/cmd/devd",
].each do |pkg|
  execute "go get -u #{pkg}" do
    creates "#{home}/.go-global/src/#{pkg}"
    environment Hash["GOPATH" => gopath]
  end
end

# Rust packages
#[
#  "racer",
#  "rustsym",
#].each do |pkg|
#  execute "cargo install #{pkg}" do
#    creates "#{home}/.cargo/bin/#{pkg}"
#  end
#end

