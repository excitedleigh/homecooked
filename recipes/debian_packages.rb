if node['platform'] == "ubuntu"
  apt_repository 'scudcloud' do
    uri 'ppa:rael-gc/scudcloud'
    distribution node['lsb']['codename']
  end
  apt_repository 'oracle-java' do
    uri 'ppa:webupd8team/java'
    distribution node['lsb']['codename']
  end
  apt_repository 'openjdk-fontfix' do
    uri 'ppa:no1wantdthisname/openjdk-fontfix'
    distribution node['lsb']['codename']
  end

  [
    'python2.7',
    'python2.7-dev',
    'mysql-server',
    'mysql-client',
    'libmysqlclient-dev',
    'phantomjs',
    'supervisor',
    'python3.4',
    'python3.4-dev',
    'python-pip',
    'git',
    'gitg',
    'postgresql',
    'libjpeg-dev', # required for pillow
    'libpq-dev', # required for psycopg2
    'ack-grep',
    'silversearcher-ag',
    'chromium-browser',
    'dos2unix',
    'npm',
    'evolution',
    'evolution-ews',
    'evolution-mapi',
    'exuberant-ctags',
    'ruby',
    'ruby-dev', # required for some rubygem packages
    'tree',
    'xclip', # required for boom
    'libxml2-dev', # required for lxml
    'libxslt1-dev', # required for lxml
    'openjdk-8-jdk', # this comes from the openjdk-fontfix ppa, and is required to make IntelliJ Platform IDEs not butt-ugly
    'inkscape',
    # 'oracle-java9-installer', # must be installed manually
  ].each do |pkg|
    package pkg
  end
  execute "update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10"
  file "/etc/profile.d/intellij-jdk.sh" do
    content "export PYCHARM_JDK=/usr/lib/jvm/java-8-openjdk-amd64/"
  end
end
