calling_user = ENV['SUDO_USER'] || node['current_user']
home = Dir.home(calling_user) #node['current_user'])

execute "git clone https://gitlab.com/abre/fishconfig.git #{home}/config/fish" do
  creates "#{home}/config/fish"
end

package 'fish'

fish_path = `which fish`.chomp!

directory "#{home}/.config"
link "#{home}/.config/fish" do
  to "#{home}/config/fish"
  action :create
end
