if node['platform'] == "ubuntu"
  apt_repository 'infinality' do
    uri 'ppa:no1wantdthisname/ppa'
    distribution node['lsb']['codename']
  end
  apt_repository 'openjdk-fontfix' do
    uri 'ppa:no1wantdthisname/openjdk-fontfix'
    distribution node['lsb']['codename']
  end
  package 'fontconfig-infinality'
  package 'openjdk-7-jdk'
  execute 'sudo /etc/fonts/infinality/infctl.sh setstyle osx'
end

USER_CONF = <<END
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fontconfig SYSTEM "../fonts.dtd">
<fontconfig>
  <match target="font">
    <edit name="antialias" mode="assign">
      <bool>true</bool>
    </edit>
    <edit name="hinting" mode="assign">
      <bool>false</bool>
    </edit>
    <edit name="autohint" mode="assign">
      <bool>false</bool>
    </edit>
    <edit name="hintstyle" mode="assign">
      <const>hintnone</const>
    </edit>
  </match>
</fontconfig>
END

if platform? "debian"
  file "/etc/fonts/conf.d/99-user.conf" do
    content USER_CONF
  end
end
