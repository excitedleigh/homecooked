home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

package 'git'

execute "git clone https://gitlab.com/abre/gitconfig.git #{home}/config/git" do
  creates "#{home}/config/git"
end

if node['kernel']['os'] == 'linux'
  package 'libgnome-keyring-dev'
  execute 'make --directory /usr/share/doc/git/contrib/credential/gnome-keyring' do
    creates '/usr/share/doc/git/contrib/credential/gnome-keyring/git-credential-gnome-keyring'
  end
end

plat = {'mac_os_x'=>'osx', 'ubuntu'=>'linux', 'debian'=>'linux'}[node['platform_family']]
file "#{home}/.gitconfig" do
    content <<-EOF
        [include]
        path = ~/config/git/config
        path = ~/config/git/config.#{plat}
        path = ~/.gitconfig_local
        [core]
        # A lot of tools that use excludesfile don't seem to support include.path
        excludesfile = ~/config/git/ignore
    EOF
end
