if platform_family? "mac_os_x"
  package 'heroku-toolbelt'
elsif platform? "ubuntu"
  execute "wget -O- https://toolbelt.heroku.com/install-ubuntu.sh | sh" do
    not_if "which heroku"
  end
end
