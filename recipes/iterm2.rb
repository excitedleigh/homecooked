calling_user = ENV['SUDO_USER'] || node['current_user']
home = Dir.home(calling_user)

link "#{home}/Library/Application Support/iTerm2/DynamicProfiles/profiles.json" do
  to "#{home}/config/iterm2profiles/profiles.json"
  action :create
end