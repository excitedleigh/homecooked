if platform? "ubuntu"
  apt_repository 'oracle-java' do
    uri 'ppa:webupd8team/java'
    distribution node['lsb']['codename']
  end
  apt_repository 'openjdk-fontfix' do
    uri 'ppa:no1wantdthisname/openjdk-fontfix'
    distribution node['lsb']['codename']
  end
end

[
  'apt-file',
  'command-not-found',
  'python2.7',
  'python2.7-dev',
  'mariadb-server',
  'libmysqlclient-dev',
  # 'phantomjs', # not available in jessie
  'python3.4',
  'python3.4-dev',
  'python-pip',
  'git',
  'gitg',
  'golang',
  'graphviz',
  'postgresql',
  'postgresql-contrib',
  'libjpeg-dev', # required for pillow
  'libpq-dev', # required for psycopg2
  'ack-grep',
  'silversearcher-ag',
  #'chromium-browser', # use chrome instead
  'dos2unix',
  'npm',
  'evolution',
  'evolution-ews',
  'evolution-mapi',
  'exuberant-ctags',
  'vagrant',
  'virtualbox',
  'redis-server',
  'ruby',
  'ruby-dev', # required for some rubygem packages
  'tree',
  'xclip', # required for boom
  'libxml2-dev', # required for lxml
  'libxslt1-dev', # required for lxml
  'libsqlite3-dev', # required for lots of stuff
  'openjdk-8-jdk', # this comes from the openjdk-fontfix ppa, and is required to make IntelliJ Platform IDEs not butt-ugly
  'inkscape',
  # 'oracle-java9-installer', # must be installed manually
].each do |pkg|
  package pkg
end
if platform? "ubuntu" or platform? "debian"
  execute "update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10"
  file "/etc/profile.d/intellij-jdk.sh" do
    content "export PYCHARM_JDK=/usr/lib/jvm/java-8-openjdk-amd64/"
  end
end

if platform? "ubuntu" or platform? "debian"
  package 'libappindicator1'
  package 'libcurl3'
  remote_file "/opt/chrome.deb" do
    source "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
  end
  dpkg_package "google-chrome" do
    source "/opt/chrome.deb"
  end
  remote_file "/opt/gitkraken.deb" do
    source "https://release.gitkraken.com/linux/gitkraken-amd64.deb"
  end
  dpkg_package "gitkraken" do
    source "/opt/gitkraken.deb"
  end
end

if platform? "ubuntu"
  remote_file "/opt/slack.deb" do
    source "https://slack-ssb-updates.global.ssl.fastly.net/linux_releases/slack-desktop-2.0.1-amd64.deb"
  end
  dpkg_package "slack-desktop" do
    source "/opt/slack.deb"
  end
end

if platform? "debian"
  package 'pkg-mozilla-archive-keyring'
  apt_repository 'mozilla' do
    uri 'http://mozilla.debian.net/'
    # this will break on testing
    distribution node['lsb']['codename'] + '-backports'
    components ['firefox-release']
  end
  package 'firefox'
end
