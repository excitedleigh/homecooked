execute 'sysctl-reload' do
  command "service procps start"
  action :nothing
end

file '/etc/sysctl.d/60-increase-user-watches.conf' do
  content "fs.inotify.max_user_watches = 524288\n"
  notifies :run, "execute[sysctl-reload]", :immediately
end
