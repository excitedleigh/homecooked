[
  # 'android-sdk',
  # 'apg',
  'ctags',
  'dash',
  # 'coconutbattery',
  # 'docker', # superceded by Docker app
  # 'docker-compose',
  # 'docker-machine',
  # 'encfs',
  'ffmpeg',
  'fish',
  'ghostscript',
  'git',
  'gitlab-runner',
  'gpg',
  'go',
  # 'heroku-toolbelt', # installed in heroku.rb
  # 'keybase', # now installed as a cask
  'imagemagick',
  'jq',
  'kotlin-compiler',
  # 'md5sha1sum', # there is a builtin shasum now
  'mobile-shell',
  'multimarkdown',
  # 'mysql',
  'node',
  'openssl',
  'pandoc',
  'p7zip',
  'postgis',
  'postgresql',
  'pwgen',
  # 'python',
  # 'python3',
  # 'python33',
  # 'python34',
  'rtmpdump',
  'redis',
  'ripgrep',
  'ruby',
  'rustup',
  'sqlite',
  'tree',
  'watchman',
  'wget',
  'xz',
].each do |pkg|
  package pkg do
    action :install
    timeout 3600
  end
end

[
  '1password',
  '1password-cli',
  'adobe-creative-cloud',
  'aerial',
  # 'android-studio',
  'appcleaner',
  'arq',
  # 'bartender',
  # 'bestres',
  'bettertouchtool',
  'caffeine',
  'chocolat',
  # 'crashplan',
  'cyberduck',
  'daisydisk',
  'docker',
  # 'dropbox',
  # 'dymo-label',
  # 'expandrive',
  'firefox',
  'fliqlo',
  # 'flux', # macOS now has Night Shift built in
  # 'focus', # now available in Setapp
  'font-aleo',
  'font-fira-code',
  'font-fira-mono',
  'font-fira-sans',
  'font-lato',
  'font-roboto-mono',
  'font-source-code-pro',
  'font-source-sans-pro',
  'font-source-serif-pro',
  'font-work-sans',
  'fontstand',
  # 'genymotion',
  'gog-galaxy',
  'gitup',
  'google-chrome',
  'handbrake',
  'hex-fiend',
  # 'haskell-platform',
  'iina',
  'imageoptim',
  # 'intel-haxm', # installed in android.rb
  # 'iterm2-beta',
  # 'itsycal',
  # 'java',
  # 'java6', # required for IntelliJ Platform IDEs to prevent discrete GPU usage
  # 'keymando', # not in index
  # 'material-colors',
  # 'minecraft',
  # 'mono-mdk', # Required for Xamarin Studio
  # 'microsoft-office',
  'keybase',
  'moom',
  'ngrok',
  # 'nylas-n1',
  # 'nimbus',
  'openemu',
  'opera',
  'psequel',
  # 'pycharm',
  'rambox',
  'sketch',
  # 'slack',
  # 'smart-scroll', # package seems to be broken
  # 'sophos-anti-virus-home-edition',
  # 'sourcetree',
  'spotify',
  'steam',
  'teamviewer',
  'transmission',
  # 'tripmode', # doesn't install, available on setapp anyway
  'vagrant',
  'virtualbox',
  'vlc',
  'vmware-fusion',
  'wkhtmltopdf',
  # 'xamarin-ios',
  # 'xamarin-studio',
  # 'xbox360-controller-driver-beta',
  # 'zotero',
].each do |cask|
  homebrew_cask cask do
    action :install
  end
end

[
  # 'mysql',
  'postgresql',
  'redis',
].each do |service|
  execute "brew services start #{service}"
end


package 'mas'

[
  '409183694',  # keynote
  '1107421413',  # 1blocker
  '803453959',  # slack
  '409201541',  # pages
  '409203825',  # numbers
].each do |appid|
  execute "mas install #{appid}"
end
