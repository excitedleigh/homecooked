# A lot of these are from here:
# https://github.com/mathiasbynens/dotfiles/blob/master/.osx
# https://github.com/hjuutilainen/dotfiles/blob/master/bin/osx-user-defaults.sh

# Global

# Set key repeat rate to be blazingly fast
execute 'defaults write NSGlobalDomain KeyRepeat -int 2'
execute 'defaults write NSGlobalDomain InitialKeyRepeat -int 15'

execute 'defaults write com.apple.universalaccess mouseDriverCursorSize -float 2.2'

# Enable debug menu in Safari
execute 'defaults write com.apple.Safari IncludeDebugMenu 1'

# Don't litter the desktop with screenshots
execute 'mkdir -p ${HOME}/Screenshots'
execute 'defaults write com.apple.screencapture location ${HOME}/Screenshots'

# Use dark menubar
# execute 'defaults write NSGlobalDomain AppleInterfaceStyle -string Dark'

# Use a 24-hour clock
execute 'defaults write NSGlobalDomain AppleICUForce12HourTime -bool false'

# Always expand the save panel
execute 'defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true'
execute 'defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true'

# Make printing apps go away after printing
execute 'defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true'

# Screensaver settings
execute 'defaults write com.apple.screensaver askForPassword -int 1'
execute 'defaults write com.apple.screensaver askForPasswordDelay -int 5'

# Activate screensaver with bottom right hot corner
execute 'defaults write com.apple.dock wvous-br-corner -int 5'
execute 'defaults write com.apple.dock wvous-br-modifier -int 0'

# Empty the Dock
execute "defaults write com.apple.Dock persistent-apps -array '()'"
execute "defaults write com.apple.Dock persistent-others -array '()'"


# Finder

execute 'defaults write com.apple.finder NewWindowTarget -string "PfLo"'
execute 'defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/"'
execute 'defaults write NSGlobalDomain AppleShowAllExtensions -bool true'
execute 'defaults write com.apple.finder ShowStatusBar -bool true'
execute 'defaults write com.apple.finder ShowPathbar -bool true'
execute 'defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"'
execute 'defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false'
execute 'defaults write com.apple.finder FXPreferredViewStyle -string "clmv"' # column view
execute 'defaults write com.apple.finder EmptyTrashSecurely -bool true'


# Chrome

execute 'defaults write com.google.Chrome AppleEnableSwipeNavigateWithScrolls -bool false'


# Transmission

execute 'defaults write org.m0k.transmission DownloadLocationConstant -bool true'
execute 'defaults write org.m0k.transmission UseIncompleteDownloadFolder -bool true'
execute 'defaults write org.m0k.transmission DownloadFolder -string "${HOME}/Documents/Torrents"'
execute 'defaults write org.m0k.transmission IncompleteDownloadFolder -string "${HOME}/Documents/Incomplete Torrents"'
execute 'defaults write org.m0k.transmission DownloadAsk -bool false'
execute 'defaults write org.m0k.transmission DeleteOriginalTorrent -bool true'
execute 'defaults write org.m0k.transmission WarningDonate -bool false'
execute 'defaults write org.m0k.transmission WarningLegal -bool false'
