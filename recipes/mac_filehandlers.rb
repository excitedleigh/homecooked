package 'duti'

DUTI_FILE = <<EOF
org.niltsh.MPlayerX .mp4 all
org.niltsh.MPlayerX .mkv all
org.niltsh.MPlayerX .webm all
org.niltsh.MPlayerX .avi all
org.niltsh.MPlayerX .flv all
org.niltsh.MPlayerX .mov all
EOF

DUTI_FILEPATH = "#{Chef::Config['file_cache_path']}/duti.txt"

file DUTI_FILEPATH do
  content DUTI_FILE
end

execute "duti #{DUTI_FILEPATH}"