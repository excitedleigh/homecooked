if platform? 'debian'
    apt_repository 'neovim' do
        uri 'http://debian.sur5r.net/neovim'
        distribution node['lsb']['codename']
        components ["main"]
        keyserver 'keyserver.ubuntu.com'
        key "E3CA1A89941C42E6"
    end
    apt_repository 'experimental' do
        uri 'http://httpredir.debian.org/debian'
        distribution 'experimental'
        components ["main"]
    end
    package 'neovim'
    package 'python3-neovim-gui'
end
