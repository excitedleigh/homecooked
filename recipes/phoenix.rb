home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

execute "git clone https://gitlab.com/abre/phoenixconfig.git #{home}/config/phoenix" do
  creates "#{home}/config/phoenix"
end

link "#{home}/Library/Application Support/Phoenix" do
  to "#{home}/config/phoenix"
  action :create
end

homebrew_cask "phoenix"
