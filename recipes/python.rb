HOME = Dir.home(ENV['SUDO_USER'] || node['current_user'])
PYTHON_VERSION = "3.7.1"
PYTHON_HOME = "#{HOME}/.pyenv/versions/#{PYTHON_VERSION}"
PYTHON_BIN = "#{PYTHON_HOME}/bin/python"

package 'pyenv'
execute 'git clone https://github.com/momo-lab/pyenv-install-latest.git "$(pyenv root)"/plugins/pyenv-install-latest' do
  creates "#{HOME}/.pyenv/plugins/pyenv-install-latest"
end
execute 'git clone https://github.com/pyenv/pyenv-virtualenv.git "$(pyenv root)"/plugins/pyenv-virtualenv' do
  creates "#{HOME}/.pyenv/plugins/pyenv-virtualenv"
end
execute "CFLAGS=\"-I$(brew --prefix openssl)/include\" LDFLAGS=\"-L$(brew --prefix openssl)/lib\" pyenv install #{PYTHON_VERSION}" do
  creates PYTHON_HOME
end
execute "pyenv virtualenv #{PYTHON_VERSION} default" do
  creates "#{PYTHON_HOME}/envs/default"
end
execute "pyenv global #{PYTHON_VERSION}/envs/default"
execute "curl https://raw.githubusercontent.com/mitsuhiko/pipsi/master/get-pipsi.py | python"
  creates "#{HOME}/.local/venvs/pipsi"
end

# Python packages
[
'autopep8',
'black',
'cookiecutter',
'httpie',
'isort',
'livestreamer',
'livedumper',
'pep257',
'pep8',
'pre-commit',
'tox',
'unp',
'yapf',
'youtube-dl',
'mycli',
'pgcli',
].each do |pkg|
    execute "pipsi install #{pkg}"
      creates "#{HOME}/.local/venvs/#{pkg}"
    end
end
