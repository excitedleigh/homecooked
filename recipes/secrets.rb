home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

link "#{home}/.gnupg" do
  to "#{home}/secrets/gnupg/"
  action :create
end

link "#{home}/.ssh" do
  to "#{home}/secrets/ssh/"
  action :create
end
