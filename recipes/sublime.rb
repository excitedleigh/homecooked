home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

if platform_family? "mac_os_x"
  sublime_path = "#{home}/Library/Application Support/Sublime Text 3"
else#if node['kernel']['os'] == "linux"
  sublime_path = "#{home}/.config/sublime-text-3"
end

directory sublime_path
directory "#{sublime_path}/Installed Packages/"
directory "#{sublime_path}/Packages/"

remote_file "#{sublime_path}/Installed Packages/Package Control.sublime-package" do
  source "https://packagecontrol.io/Package%20Control.sublime-package"
end

execute "git clone https://gitlab.com/abre/sublimeconfig.git #{home}/config/sublime" do
  creates "#{home}/config/sublime"
end

link "#{sublime_path}/Packages/User" do
  to "#{home}/config/sublime"
  action :create
end

if platform_family? "mac_os_x"
  homebrew_cask 'sublime-text'
  execute "defaults write com.sublimetext.3 ApplePressAndHoldEnabled -bool false"
elsif platform? "ubuntu"
  apt_repository 'sublime-text-3' do
    uri 'ppa:webupd8team/sublime-text-3'
    distribution node['lsb']['codename']
  end
  package 'sublime-text-installer'
elsif node['os'] == "linux"

  remote_file "/opt/sublime.tbz2" do
    source "https://download.sublimetext.com/sublime_text_3_build_3114_x64.tar.bz2"
    action :create
  end
  execute "tar -xf /opt/sublime.tbz2 && mv /opt/sublime_text_3 /opt/sublime_text" do
    cwd "/opt"
    creates "/opt/sublime_text"
    notifies :create, 'remote_file[/opt/sublime.tbz2]', :before
  end
  link '/usr/local/bin/subl' do
    to "/opt/sublime_text/sublime_text"
  end
end
