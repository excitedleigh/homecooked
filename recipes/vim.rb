home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

execute "git clone https://gitlab.com/abre/vimconfig.git #{home}/config/vim" do
    creates "#{home}/config/vim"
end

link "#{home}/.vim" do
    to "#{home}/config/vim"
    action :create
end

case node["platform"]
when "ubuntu"
    package 'vim-gnome'
    package 'cmake' # required to build YouCompleteMe
end
if platform_family? "mac_os_x"
    ['vim', 'macvim'].each do |pkg|
        package pkg do
            action :install
            options '--with-python3 --with-luajit'
        end
    end
end

# this gives an error message about missing colourschemes with a 'Press ENTER
# to continue' message
# execute "vim +PlugInstall +qa"
