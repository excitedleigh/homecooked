home = Dir.home(ENV['SUDO_USER'] || node['current_user'])

if platform? "ubuntu" or platform? "debian"
  remote_file "/opt/vscode.deb" do
    source "https://go.microsoft.com/fwlink/?LinkID=760868"
  end
  dpkg_package "visual-studio-code" do
    source "/opt/vscode.deb"
  end
  VSC_USER_PATH = "#{home}/.config/Code/User"
  execute "mkdir -p '#{home}/.config/Code'"
elsif platform_family? "mac_os_x"
  homebrew_cask 'visual-studio-code'
  VSC_USER_PATH = "#{home}/Library/Application Support/Code/User"
  execute "mkdir -p '#{home}/Library/Application Support/Code'"
end

execute "git clone https://gitlab.com/abre/vscodeconfig.git #{home}/config/vscode" do
  creates "#{home}/config/vscode"
end

link VSC_USER_PATH do
  to "#{home}/config/vscode"
  action :create
end

#DESKTOP_FILE = <<EOS
#[Desktop Entry]
#Type=Application
#Version=1.0
#Name=Visual Studio Code
#Exec=/opt/vscode/code
#Icon=/opt/vscode/resources/app/resources/linux/code.png
#Terminal=false
#EOS
#
#file '/usr/share/applications/visual-studio-code.desktop' do
#  content DESKTOP_FILE
#end
