#!/bin/bash
set -e # terminate on error

if [[ `uname` == "Darwin" ]]; then
    export SUDO_ASKPASS=`pwd`/askpass.applescript
    # Bootstrap the Chef DK so that we can use Chef to install everything else.
    if ! which berks; then
        if ! which brew; then
            ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
            # The included recipes will happily install Homebrew by themselves, but
            # we need it to be present in order to install
        fi
        brew tap caskroom/cask
        brew cask install chefdk
    fi
elif [[ `uname` == "Linux" ]]; then
    if ! which berks; then
        DISTRO=debian
        DISTRO_VERSION=8
        VERSION=0.11.2-1
        CHEFPKG=chefdk_${VERSION}_amd64.deb
        URL="https://opscode-omnibus-packages.s3.amazonaws.com/$DISTRO/$DISTRO_VERSION/x86_64/$CHEFPKG"
        echo "Downloading $URL"
        wget $URL
        sudo dpkg -i $CHEFPKG
        rm $CHEFPKG
    fi
fi
rm -rf cookbooks
berks vendor cookbooks
exec chef-client --local-mode --runlist 'homecooked'
